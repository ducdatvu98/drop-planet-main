using TMPro;
using UnityEngine;

public class BonusController : MonoBehaviour
{
    public TextMeshProUGUI textBonus;
    public TextMeshProUGUI textBonus2;
    public TextMeshProUGUI textBonus3;

    void Update()
    {
        _SetBonusText(GamePlayController._GamePL.bonus);
        textBonus.transform.Translate(Vector3.up * 1.5f * Time.deltaTime);
        textBonus2.transform.Translate(Vector3.up * 1.5f * Time.deltaTime);
        textBonus3.transform.Translate(Vector3.up * 1.5f * Time.deltaTime);
    }

    void _SetBonus()
    {
        if (GamePlayController._GamePL.canbunus == true)
        {
            Vector3 position = transform.position;
            float   radius   = 1.0f;
            float   angle    = Random.Range(0.0f, 360.0f);
            Vector3 offset   = new Vector3(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle), 0.0f);
            position                            += offset;
            textBonus.transform.position        =  position;
            GamePlayController._GamePL.canbunus =  false;
        }
    }

    void _SetBonus2()
    {
        if (GamePlayController._GamePL.canbunus == true)
        {
            Vector3 position = transform.position;
            float   radius   = 1.0f;
            float   angle    = Random.Range(0.0f, 360.0f);
            Vector3 offset   = new Vector3(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle), 0.0f);
            position                            += offset;
            textBonus2.transform.position       =  position;
            GamePlayController._GamePL.canbunus =  false;
        }
    }

    void _SetBonus3()
    {
        if (GamePlayController._GamePL.canbunus == true)
        {
            Vector3 position = transform.position;
            float   radius   = 1.0f;
            float   angle    = Random.Range(0.0f, 360.0f);
            Vector3 offset   = new Vector3(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle), 0.0f);
            position                            += offset;
            textBonus3.transform.position       =  position;
            GamePlayController._GamePL.canbunus =  false;
        }
    }

    void _SetBonusText(int bonus)
    {
        switch (bonus)
        {
            case 5:
                textBonus.text = "Nice!";
                _SetBonus();

                break;
            case 6:
                textBonus2.text = "Like!";
                _SetBonus2();

                break;
            case 7:
                textBonus3.text = "Good!";
                _SetBonus3();

                break;
            case 8:
                textBonus.text = "Great!";
                _SetBonus();

                break;
            case 9:
                textBonus2.text = "Amazing!!";
                _SetBonus2();

                break;
            case 10:
                textBonus3.text = "LEGENDARY!!!";
                _SetBonus3();

                break;
            default:
                break;
        }
    }
}