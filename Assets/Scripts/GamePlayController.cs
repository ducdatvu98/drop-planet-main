using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayController : MonoBehaviour
{
    public                   GameObject         overPanel;
    public                   GameObject         settingPanel;
    public                   Slider             volumeSlider;
    [SerializeField] private TextMeshProUGUI    scoreText, bestScoreText;
    private                  int                score      = 0;
    private const            string             HIGH_SCORE = "High Score";
    public                   int[]              count      = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    public                   int                bonus;
    public                   bool               canbunus = false;
    public                   AudioClip          mergeClip, endClip, spawnClip;
    public                   AudioSource        audioSource;
    public static            bool               canMerge = true;
    public static            GamePlayController _GamePL;

    void Awake()
    {
        _GamePL = this;
        _IsGameStartedForTheFirstTime();
    }

    void Start()
    {
        audioSource.Play();

        if (volumeSlider != null && audioSource != null)
        {
            volumeSlider.value = audioSource.volume;
            volumeSlider.onValueChanged.AddListener(delegate { _OnVolumeChange(); });
        }
    }

    void FixedUpdate()
    {
        _TotalScore();
        _SetScore(score);
    }

    void _IsGameStartedForTheFirstTime()
    {
        if (!PlayerPrefs.HasKey("IsGameStartedForTheFirstTime"))
        {
            PlayerPrefs.SetInt(HIGH_SCORE, 0);
            PlayerPrefs.SetInt("IsGameStartedForTheFirstTime", 0);
        }
    }

    public void _TotalScore()
    {
        int scoreT = 0;

        for (int i = 0; i < count.Length; i++)
        {
            scoreT += count[i] * (i + 1);
        }

        score = scoreT;
    }

    public void _SetScore(int score)
    {
        scoreText.text = "" + score;

        if (score > _GetHighScore())
        {
            _SetHighScore(score);
        }

        bestScoreText.text = "" + _GetHighScore();
    }

    public void _SetHighScore(int score) { PlayerPrefs.SetInt(HIGH_SCORE, score); }
    public int  _GetHighScore()          { return PlayerPrefs.GetInt(HIGH_SCORE); }
    public void _ResetMerge()            { Invoke(nameof(_TrueMerge), 0.01f); }
    void        _TrueMerge()             { canMerge = true; }

    //Buton and Menu --------------------------------------
    public void _playGame()
    {
        Time.timeScale = 1;
        overPanel.SetActive(false);
        SceneManager.LoadScene("Play");
    }

    public void _MenuButton() { SceneManager.LoadScene(0); }

    public void _SettingButton()
    {
        settingPanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void _ContinueButton()
    {
        settingPanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void _OnVolumeChange()
    {
        if (audioSource != null)
        {
            audioSource.volume = volumeSlider.value;
        }
    }
}