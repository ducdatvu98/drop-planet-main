using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    // Start is called before the first frame update
    public void _PlayButton()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

    public void _ExitGame() { Application.Quit(); }
}