using System.Collections;
using UnityEngine;

public class Merge : MonoBehaviour
{
    void Awake()
    {
        Physics2D.SyncTransforms();
        StartCoroutine(_IncreaseRadiusAfterDelay());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (GamePlayController.canMerge && gameObject.CompareTag(collision.gameObject.tag))
        {
            int i = int.Parse(gameObject.tag);
            GamePlayController._GamePL.count[i - 1]++;

            if (GamePlayController._GamePL.count[i - 1] % 2 == 0)
            {
                GameObject spawnedObject = Instantiate(SpawnController._SpawnCl.objectsToSpawn[i], collision.transform.position, Quaternion.identity);
                GamePlayController._GamePL.audioSource.PlayOneShot(GamePlayController._GamePL.mergeClip);
                GamePlayController.canMerge = false;
                GamePlayController._GamePL._ResetMerge();
                GamePlayController._GamePL.bonus    = i;
                GamePlayController._GamePL.canbunus = true;
            }

            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("end"))
        {
            GamePlayController._GamePL.audioSource.PlayOneShot(GamePlayController._GamePL.endClip);
            Time.timeScale = 0;
            GamePlayController._GamePL.overPanel.gameObject.SetActive(true);
        }
    }

    private IEnumerator _IncreaseRadiusAfterDelay()
    {
        gameObject.transform.localScale -= Vector3.one * 0.05f;

        yield return new WaitForSeconds(0.001f);
        gameObject.transform.localScale += Vector3.one * 0.05f;
    }
}