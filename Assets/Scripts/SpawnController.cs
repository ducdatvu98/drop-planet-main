using System.Collections;
using UnityEngine;
public class SpawnController : MonoBehaviour
{
    public        GameObject[]    objectsToSpawn;
    private       float           spawnHeight = 4.5f; 
    private       bool            canClick    = true;
    private       Vector3         mouseP;
    private       Camera          mainCamera;
    private       int             Random04;
    public static SpawnController _SpawnCl;
    void                          Awake() { _SpawnCl = this; }

    void Start()
    {
        ChangeSprite();
        mainCamera = Camera.main;
        StartCoroutine(UpdateMousePosition());
    }

    void Update()
    {

        transform.position = new Vector2(mouseP.x, transform.position.y);

        if (canClick && Input.GetMouseButtonUp(0))
        {
            GamePlayController._GamePL.audioSource.PlayOneShot(GamePlayController._GamePL.spawnClip);
            SpawnRandomObject(mouseP);
            RemoveSprite();
            RandomObject();
            Invoke(nameof(ChangeSprite), 0.7f);
            Invoke(nameof(ResetClick), 0.7f);
            canClick = false;
        }
    }

    private IEnumerator UpdateMousePosition()
    {
        while (true)
        {

            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                mouseP = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, Camera.main.nearClipPlane));

            }

            yield return null;
        }
    }

    void SpawnRandomObject(Vector3 touchPoint)
    {
        GameObject spawnedObject = Instantiate(objectsToSpawn[Random04], new Vector3(touchPoint.x, spawnHeight, 0f), Quaternion.identity);
    }

    void RandomObject() { Random04 = Random.Range(0, 4); }

    void ChangeSprite()
    {
        SpriteRenderer sourceSpriteRenderer = objectsToSpawn[Random04].GetComponent<SpriteRenderer>();
        SpriteRenderer targetSpriteRenderer = GetComponent<SpriteRenderer>();
        RectTransform  targetTransform      = GetComponent<RectTransform>();

        if (sourceSpriteRenderer != null && targetSpriteRenderer != null)
        {
            targetSpriteRenderer.sprite = sourceSpriteRenderer.sprite;
            targetSpriteRenderer.size   = sourceSpriteRenderer.size;
            targetTransform.localScale  = objectsToSpawn[Random04].transform.localScale;
        }
    }

    void RemoveSprite()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        if (spriteRenderer != null)
        {
            spriteRenderer.sprite = null;
        }
    }

    void ResetClick() { canClick = true; }
}